from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty
from kivy.utils import get_hex_from_color
from kivy.uix.boxlayout import BoxLayout
from kivymd.theming import ThemeManager
from android.runnable import run_on_ui_thread
from jnius import autoclass


Color = autoclass("android.graphics.Color")
WindowManager = autoclass('android.view.WindowManager$LayoutParams')
activity = autoclass('org.kivy.android.PythonActivity').mActivity


widget_kv = """
MainWindow

<MainWindow>
    white: 1,1,1,1
    orientation: "vertical"
    w: id_weight
    h: id_height

    MDToolbar
        title: "Calculadora de IMC"
        anchor_title: "center"
        specific_text_color: root.white
        md_bg_color: app.theme_cls.primary_color
        right_action_items: [['reload', lambda x: root.reset_fields()]]

    BoxLayout
        orientation: "vertical"
        padding: 20, 0
        spacing: dp(20)

        MDIcon
            size_hint_y: None
            icon: "account-outline"
            font_size: dp(120)
            size: self.texture_size
            halign: "center"
            theme_text_color: "Custom"
            text_color: app.theme_cls.primary_color

        MyMDTextField
            id: id_weight
            hint_text: "Peso (KG)"
            font_size: dp(25)          
            helper_text: "Insira seu peso"
            foreground_color: app.theme_cls.primary_color
            
        MyMDTextField
            id: id_height
            font_size: dp(25)
            hint_text: "Altura (Cm)"
            helper_text: "Insira sua altura"
            foreground_color: app.theme_cls.primary_color

        MDRaisedButton
            size_hint: 1, None
            text: "Calcular"
            font_size: dp(14)
            height: dp(46)
            on_press: root.calculate()
            text_color: root.white

        MDLabel
            text: root.info_text
            font_size: dp(25)
            halign: "center"
            theme_text_color: "Custom"
            text_color: app.theme_cls.primary_color
            size_hint_y: None
            size: self.texture_size

        Space


<MyMDTextField@MDTextField>
    required: True
    helper_text_mode: "on_error"
    halign: "center"
    line_color_normal: app.theme_cls.text_color   
    hint_text_color: app.theme_cls.primary_color
    input_filter: "int"
    size_hint_y: None
    height: dp(10)
    _hint_lbl_font_size: dp(25)
    _current_hint_text_color: app.theme_cls.primary_color
    on_focus: Animation(_current_hint_text_color=app.theme_cls.primary_color, \
        _hint_lbl_font_size=dp(25)).start(self)

<Space@BoxLayout>
"""


class MainWindow(BoxLayout):
    w = ObjectProperty()
    h = ObjectProperty()
    info_text = StringProperty("Informe seus dados")

    def calculate(self):
        if self.w.text and self.h.text:

            weight = float(self.w.text)
            height = float(self.h.text)/100
            imc = weight/(height*height)

            if imc < 18.6:
                info_text = f"Abaixo do Peso ({format(imc, '.2f')})"
            elif imc >= 18.6 and imc < 24.9:
                info_text = f"Peso Ideal ({format(imc, '.2f')})"
            elif imc >= 24.9 and imc < 29.9:
                info_text = f"Levemente Acima do Peso ({format(imc, '.2f')})"
            elif imc >= 29.9 and imc < 34.9:
                info_text = f"Obesidade Grau I ({format(imc, '.2f')})"
            elif imc >= 34.9 and imc < 39.9:
                info_text = f"Obesidade Grau II ({format(imc, '.2f')})"
            elif imc >= 40:
                info_text = f"Obesidade Grau III ({format(imc, '.2f')})"

            self.info_text = info_text

    def reset_fields(self):
        self.w.text = ""
        self.h.text = ""
        self.info_text = "Informe seus dados"


class CalculadoraIMC(App):
    title = "Calculadora de IMC"
    theme_cls =ThemeManager(primary_palette="Green")
    
    def build(self):
        color = get_hex_from_color(self.theme_cls.primary_color[:3])
        self.set_statusbar_color(color)
        return Builder.load_string(widget_kv)

    @run_on_ui_thread
    def set_statusbar_color(self, color):
        window = activity.getWindow()
        window.clearFlags(WindowManager.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.setStatusBarColor(Color.parseColor(color)) 


if __name__ == '__main__':
    CalculadoraIMC().run()